# team-lead-test

## Project setup
```
npm install
```
### Start JSON server
```
json-server --watch db.json
```
### Open another terminal and run the app
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
