import Vue from "vue";
import App from "./App.vue";
import Vuex from "vuex";
import VueRouter from "vue-router";

import axios from "axios";
import VueAxios from "vue-axios";

import Buefy from "buefy";
import "buefy/dist/buefy.css";

import bButton from "buefy/src/components/button/Button.vue";
import bLoading from "buefy/src/components/loading/Loading.vue";
import bPagination from "buefy/src/components/pagination/Pagination.vue";
import bSelect from "buefy/src/components/select/Select.vue";
import field from "buefy/src/components/field/Field.vue";
import input from "buefy/src/components/input/Input.vue";

import "@mdi/font/css/materialdesignicons.css";

import { library } from "@fortawesome/fontawesome-svg-core";
import { faSignLanguage } from "@fortawesome/free-solid-svg-icons";
import { dom } from "@fortawesome/fontawesome-svg-core";
dom.watch();
library.add(faSignLanguage);

import VueSweetalert2 from "vue-sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";

Vue.use(VueSweetalert2);

import VueLodash from "vue-lodash";
import lodash from "lodash";

import "./vee-validate";

import router from "./router";
import store from "./store";

Vue.component("b-button", bButton);
Vue.component("b-loading", bLoading);
Vue.component("b-pagination", bPagination);
Vue.component("b-select", bSelect);
Vue.component("b-field", field);
Vue.component("b-input", input);

Vue.use(Vuex, VueRouter, VueAxios, axios, Buefy, VueLodash, {
  name: "custom",
  lodash: lodash
});
Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  router,
  store,
  created() {
    this.$store.dispatch("getPosts");
  }
}).$mount("#app");
