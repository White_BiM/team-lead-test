import Vue from "vue";
import AuthGuard from "./auth-guard";
import Router from "vue-router";
import Posts from "@/pages/Posts";
import NewPost from "@/pages/NewPost";
import LogIn from "@/pages/LogIn";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    { path: "/", redirect: "/posts" },
    {
      path: "/posts",
      name: "Posts",
      component: Posts
    },
    {
      path: "/posts/new",
      props: true,
      name: "New post",
      component: NewPost,
      beforeEnter: AuthGuard
    },
    {
      path: "/posts/edit/:id",
      props: true,
      name: "Edit post",
      component: NewPost,
      beforeEnter: AuthGuard
    },
    {
      path: "/login",
      name: "Log in",
      component: LogIn
    }
  ],
  scrollBehavior() {
    return { x: 0, y: 0 };
  }
});
