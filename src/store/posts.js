import axios from "axios";
import { Swal } from "../sweet-alert";
const HTTP = axios.create({
  baseURL: "http://localhost:3000/"
});

export default {
  state: {
    loading: false,
    posts: []
  },
  mutations: {
    setLoading(state, payload) {
      state.loading = payload;
    },
    setPosts(state, payload) {
      state.posts = payload;
    },
    addPost(state, post) {
      state.posts.push(post);
    },
    deletePost(state, postId) {
      const myArray = state.posts.filter(function(post) {
        return post.id !== postId;
      });
      state.posts = myArray;
    },
    editPost(state, updatedPost) {
      const post = state.posts.find(post => String(post.id) === updatedPost.id);
      post.title = updatedPost.title;
      post.description = updatedPost.description;
      post.updateAt = updatedPost.updateAt;
    },
    addClaps(state, updatedPost) {
      const post = state.posts.find(post => post.id === updatedPost.id);
      post.claps = updatedPost.claps;
    }
  },

  actions: {
    setLoading({ commit }, payload) {
      commit("setLoading", payload);
    },
    async getPosts({ commit }) {
      commit("setLoading", true);
      try {
        await HTTP.get("posts").then(response => {
          commit("setPosts", response.data);
          commit("setLoading", false);
        });
      } catch (error) {
        commit("setLoading", false);
        Swal(`${error}`, "error");
        throw error;
      }
    },
    async addPost({ state, commit }, formData) {
      commit("setLoading", true);
      try {
        formData.createdAt = formData.updateAt = new Date().toISOString();
        formData.id = state.posts.length + 1;

        await HTTP.post("posts", formData);
        commit("addPost", formData);
        commit("setLoading", false);
        Swal("Created!", "success", "Your post has been added.");
      } catch (error) {
        commit("setLoading", false);
        Swal(`${error}`, "error");
        throw error;
      }
    },
    async deletePost({ commit }, postId) {
      commit("setLoading", true);
      try {
        await HTTP.delete(`posts/${postId}`);
        commit("deletePost", postId);
        commit("setLoading", false);
        Swal("Deleted!", "success", "Your post has been deleted.");
      } catch (error) {
        commit("setLoading", false);
        Swal(`${error}`, "error");
        throw error;
      }
    },
    async editPost({ commit }, formData) {
      commit("setLoading", true);
      try {
        formData.updateAt = new Date().toISOString();

        await HTTP.patch(`posts/${formData.id}`, formData);
        commit("editPost", formData);
        commit("setLoading", false);
        Swal("Edited!", "success", "Your post has been edited.");
      } catch (error) {
        commit("setLoading", false);
        Swal(`${error}`, "error");
        throw error;
      }
    },
    async addClaps({ commit }, data) {
      try {
        await HTTP.patch(`posts/${data.id}`, data);
        commit("addClaps", data);
      } catch (error) {
        Swal(`${error}`, "error");
        throw error;
      }
    }
  },

  getters: {
    posts(state) {
      return state.posts;
    },
    loading(state) {
      return state.loading;
    },
    postById(state) {
      return postId => {
        return state.posts.find(post => String(post.id) === postId);
      };
    }
  }
};
