import { Swal } from "../sweet-alert";
import axios from "axios";
const HTTP = axios.create({
  baseURL: "http://localhost:3000/"
});

export default {
  state: {
    user: null
  },
  mutations: {
    setUser(state, payload) {
      state.user = payload;
    }
  },

  actions: {
    async logIn({ commit }, formData) {
      commit("setLoading", true);
      try {
        let user = {};
        await HTTP.get(`users?login=${formData.login}`).then(response => {
          user = response.data[0];
        });
        if (user && String(user.password) === formData.password) {
          Swal(`Logged in as ${user.role}`, "success");
          commit("setUser", user);
        } else {
          Swal("Cannot log in with this data", "error");
        }
        commit("setLoading", false);
      } catch (error) {
        commit("setLoading", false);
        Swal(`${error}`, "error");
        throw error;
      }
    },
    async logOut({ commit }) {
      commit("setLoading", true);
      try {
        commit("setUser", null);
        Swal("Logged Out", "warning");
        commit("setLoading", false);
      } catch (error) {
        commit("setLoading", false);
        Swal(`${error}`, "error");
        throw error;
      }
    }
  },

  getters: {
    user(state) {
      return state.user;
    }
  }
};
