import Vue from "vue";

export const Swal = function(message, type, text) {
  Vue.swal.fire({
    title: message,
    text: text,
    icon: type
  });
};
